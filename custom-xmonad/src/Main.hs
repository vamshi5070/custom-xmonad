import XMonad
import qualified XMonad as X
-- import Data.Monoid
import System.Exit
import qualified XMonad.StackSet as W

-- Data
import Data.Maybe (fromJust)
import qualified Data.Map as M

import XMonad.Actions.Commands
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Search
import XMonad.Actions.Submap
import XMonad.Actions.WindowGo

       -- hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (avoidStruts,docks, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.SetWMName

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

  --prompts
import XMonad.Prompt
import XMonad.Prompt.AppLauncher as AL
import XMonad.Prompt.ConfirmPrompt
-- import XMonad.Prompt.OrgMode
import XMonad.Prompt.Shell
import XMonad.Prompt.Window
import XMonad.Prompt.XMonad

import XMonad.Util.Dmenu
import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
dswitcher = "~/aios/dmenu/windowSwitcher.sh"
xmenu = "~/aios/xmenu/xmenu.sh"

-- clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
--     where i = fromJust $ M.lookup ws myWorkspaceIndices

-- myWorkspaces = [" dev ", " www ", " sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx "]
myWorkspaces = ["arr", "ir"] -- ," sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

myMainKey = "M-"

myEditor = "emacsclient -c -a emacs"
myTerminal      = "kitty" --"alacritty"
-- myTerminal      = "alacritty"
myBrowser =  "firefox" --""
-- myTerminal      = "alacritty"
-- myFocusColor  = "#46d9ff"   -- Border color of focused windows

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- alttab = "alttab -font  \"xft:consolas-20\"&"

myNormColor :: String
myNormColor   = "#282c34"   -- Border color of normal windows

myFocusColor :: String
myFocusColor  = "#46d9ff"   -- Border color of focused windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

projects :: [Project]
projects =
  [ Project { projectName      = "arr"
            , projectDirectory = "~/"
            , projectStartHook = Nothing
            }

  , Project { projectName      = "ir"
            , projectDirectory = "~/"
            , projectStartHook = Nothing
              --Just $ do spawn "conkeror"
                --                           spawn "chromium"
            }
  ]

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [
     -- Rotate through the available layout algorithms
    -- , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size

    -- Move focus to the next window
    -- , ((modm,               xK_Tab   ), windows W.focusDown)

    -- , ((modm .|. shiftMask, xK_Tab     ), windows W.focusUp  )

    -- Move focus to the master window
    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- , ((modm , xK_Return ) , return ())

    -- , ((modm , ))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad

    -- Restart xmonad
    , ((modm  .|. shiftMask , xK_r     ), spawn "xmonad --recompile; xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    -- , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

    -- Swap the focused window and the master window
    -- , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    -- Shrink the master area

    -- Expand the master area

    -- Push window back into tiling

myDmenu = menuArgs "dmenu" ["-l","10","-n","-fn","Lucida MAC:size=40"]

miscKeys :: [(String,String,X())]
miscKeys = [--("C-<Space>" ,"" ,spawn dswitcher)
           ("M1-<Space>", "infinte recursion",runCommandConfig  myDmenu . skip1element $ ezKeys <> miscKeys )
          , ("M1--","sound decrease",spawn "amixer set Master 10%- unmute")
          ,("M1-<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
          , ("M1-=","sound decrease",spawn "amixer set Master 10%+ unmute")           ]

skip2element xs = [(p,r)| (p,q,r) <- xs]

skip1element xs = [(q,r)| (p,q,r) <- xs]

makeGrid2 = runSelectedAction (gsconfig2 myColorizer)

ezKeys :: [(String,String,X())]
ezKeys = [
  ("S-]","shiftToNext ", shiftToNext )
 ,("<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
  -- KB_GROUP Layouts
  , ("<Tab>", "nextLayout", sendMessage NextLayout)           -- Switch to next layout
  , ("f", "fullScreen",sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  , ("S-f", "fullScreen", sendMessage ToggleStruts) -- Toggles noborder/full
  -- ,("S-<Return>" ,"spawnOn", shellPromptOn "5" shellXPConfig  )
  ,("S-c","kill",kill)
  ,("S-q","xmonad --logout",io (exitWith ExitSuccess))
  ,("w","windows", alttab)--goToSelected def {gs_cellheight = 100,gs_cellwidth=400,gs_font="xft:Lucida MAC:size=24"})
  ,("e","ground",myGround)
  ,("a","apps",makeGrid2 appsList)
  -- ,("r","redshift",)
  ,("r","redshift",  makeGrid2 redList)
  ,("S-e","exit",  makeGrid2 exitList)
  ,("n","nixos",makeGrid2 nixosList)
  ,("S-[", "shiftToPrev ",shiftToPrev )--sendMessage Shrink)
  ,("S-k","windows W.swapUp",windows W.swapUp)
  ,("S-j","windows W.swapDown" ,windows W.swapDown)
  ,("u","add workspaces",switchProjectPrompt googleXPConfig  )
  -- ,("g","google search",spawn "~/aios/dmenu/google")
  ,("g" ,"google", promptSearchBrowser googleXPConfig myBrowser google)--spawn "~/vc/firefox/google.sh")
  ,("y" ,"youtube", promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")
  -- ,("y","youtube search",spawn "~/aios/dmenu/youtube")
  -- ,("f","files", spawn "~/aios/dmenu/files.sh")
  ,("1","editor emacs", raiseNextMaybe  (confirmPrompt googleXPConfig "Emacs" $ spawn myEditor)(className =? "Emacs"))
  ,("2" , "browser", raiseNextMaybe (xmonadPromptC webList keyXPConfig) (className =? "Firefox")) -- Firefox
  -- ,("3","terminal", raiseNextMaybe  (spawn "alacritty")(className =? "Alacritty"))
  ,("3","pdf reader zathura", raiseNextMaybe (AL.launchApp zathuraXPConfig "zathura") (className =? "Zathura"))
  ,("4","spotify",raiseNextMaybe (confirmPrompt googleXPConfig "spotify" $ spawn "spotify")(className =? "Spotify") )
  ,("5","alacritty",raiseNextMaybe (confirmPrompt googleXPConfig "alacritty" $ spawn "alacritty")(className =? "Alacritty") )
  -- ,("2","browser firefox" , raiseNextMaybe (spawn "~/aios/dmenu/input") (className =? "Firefox")) -- Firefox
  -- ,("1","editor emacs" , (spawn "~/aios/dmenu/emacsFiles.sh") (className =? "Emacs"))
  ,("m", "focus master",windows W.focusMaster)
  ,("[","previous workspace",prevWS)
  -- , ("e","testing", confirmPrompt def "shutdown" $ spawn "lxappearance")  -- ,("q","testing",prompt ("emacsclient -c -a" ++ " emacs ") greenXPConfig)
  ,("q","shellprompt",shellPrompt shellXPConfig)
  ,("]","next workspace",nextWS)
  -- ,("n","refresh windows size" ,refresh)
  ,("t","force tiling", withFocused $ windows . W.sink)
  ,("j","windows focus down",windows W.focusDown)
  ,("k","windows focus up",windows W.focusUp)
  ,("p","app launcher",spawn myLauncher)
  ,("o","window Switcher",windowPrompt windowXPConfig Goto windowMap')
  -- ,("o","window switcher",spawn dswitcher)
  -- ,("<Space>","next layout",sendMessage NextLayout)
  -- "M-S-<Space>",sendMessage PrevLayout)
  -- "M-<Return>", spawn myTerminal)-- $ XMonad.terminal conf)

  ,("`","desktop",spawn xmenu)]

mainTurner :: [a] -> [([a], b)] -> [([a], b)]
mainTurner key xs = [(key <> p,q)|(p,q) <- xs]

myLauncher = "dmenu_run -n -fn \"Consolas:bold:size=34\""
    -- launch dmenu
webList = [
  ("google" , promptSearchBrowser googleXPConfig myBrowser google)--spawn "~/vc/firefox/google.sh")
  ,("youtube" , promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")
          ]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]
  <+> namedScratchpadManageHook myScratchPads
 
--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Consolas:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout = withBorder myBorderWidth tall
                                 ||| magnify
                                 ||| floats
                                 ||| noBorders monocle
                                 ||| grid
                                 ||| noBorders tabs
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion
------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = handleEventHook def <+> fullscreenEventHook --mempty

myStartupHook = do
  -- spawnOnce $ alttab
  spawnOnce "brightnessctl s 1"
  -- spawnOnce "systemctl --user start taffybar.service"

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do
    xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/.xmobarrc"
    -- xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/doom-one-xmobarrc"
    -- xmproc2 <- spawnPipe "xmobar -x 2 $HOME/.config/xmobar/doom-one-xmobarrc"
    xmonad $ docks $ ewmh def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormColor,
        focusedBorderColor = myFocusColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayoutHook, --showWName' myShowWNameTheme $  mylayouthook,
        manageHook         = myManageHook <+> manageDocks,
        handleEventHook    = docksEventHook <+> myEventHook,
        -- logHook            = myLogHook,
        startupHook        = myStartupHook,
        logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
              -- the following variables beginning with 'pp' are settings for xmobar.
              { ppOutput = \x -> hPutStrLn xmproc0 x                          -- xmobar on monitor 1
                              -- >> hPutStrLn xmproc1 x                          -- xmobar on monitor 2
                              -- >> hPutStrLn xmproc2 x                          -- xmobar on monitor 3
              , ppCurrent = xmobarColor "#c594c5" "" . wrap "<box type=Bottom width=2 mb=2 color=#c792ea>""</box>"         -- Current workspace
              , ppVisible = xmobarColor "#6699cc" "" -- . clickable              -- Visible but not current workspace
              , ppHidden = xmobarColor "#82AAFF" "" . wrap "<box type=Top width=2 mt=2 color=#82AAFF>""</box>" --  . clickable -- Hidden workspaces
              , ppHiddenNoWindows = xmobarColor "#82AAFF" "" --  . clickable     -- Hidden workspaces (no windows)
              , ppTitle = xmobarColor "#b3afc2" "" . shorten 60               -- Title of active window
              , ppSep =  "<fn=1>  </fn>"                    -- Separator character
              , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"            -- Urgent workspace
              , ppExtras  = [windowCount]                                     -- # of windows current workspace
              , ppOrder  = \(ws:l:t:ex) -> [t] --[ws,l]++ex++[t]                    -- order of things in xmobar
              }    }
           `additionalKeysP` (emacsList <> addKeys <> mainTurner myMainKey (skip2element ezKeys) <> skip2element miscKeys)

addKeys = [ ("M-i", modalmap . M.fromList $
          [ ((0, xK_r), spawn "emacs")
          , ((0, xK_w), spawn "firefox")
          , ((0, xK_s), spawn "rofi -show ssh -show-icons")
          , ((0, xK_d), spawn "rofi -show drun -show-icons")
          , ((shiftMask, xK_w), spawn "rofi -show windowcd -show-icons")
          , ((0, xK_c), spawn "rofi -show combi -show-icons")
          , ((0, xK_k), spawn "rofi -show keys -show-icons")
          ])
      ]

emacsList = [("S-<Return> e" ,spawn myEditor)
            ,("S-<Return> k", kill)
            ,("S-<Return> f", spawn "emacsclient -c -a '' --eval '(dired nil)'")
            ,("S-<Return> n u", spawn "home-manager switch --flake \"/home/vamshi/aios#cosmos\" && notify-send \"Successfully updated\" || notify-send -u critical \"Updating failed!!\" ")
            -- ,("C-z ")
            ]

modalmap :: M.Map (KeyMask, KeySym) (X ()) -> X ()
modalmap s = submap $ M.map (\x -> x >> modalmap s) s

shellXPConfig = greenXPConfig {
  autoComplete      = Nothing--Just 100000    -- set Just 100000 for .1 sec
  , height            = 70
  ,promptKeymap = emacsLikeXPKeymap
  , showCompletionOnTab = False
  ,  position = Top
      --CenteredAt {xpCenterY = 0.19 , xpWidth = 0.88}
  ,  font = "xft:Lucida MAC:size=20"
  ,defaultPrompter = const ">>= "
  ,fgColor = myFocusColor
  }

windowXPConfig = greenXPConfig {
   font = "xft:Lucida MAC:size=20"
   , height            = 70
   , autoComplete      = Just 100000    -- set Just 100000 for .1 sec
   ,  position = CenteredAt {
       xpCenterY = 0.19 , xpWidth = 0.88
       }
                               }
emacsXPConfig = shellXPConfig {
  defaultPrompter = const "Emacs: "
                    -- ,defaultText = "emacsclient -c -a emacs "
  }
zathuraXPConfig = shellXPConfig {
  defaultPrompter = const "Zathura: "
  }
keyXPConfig = googleXPConfig {
  autoComplete = Just 100000
  , fgColor = myFocusColor
  , borderColor = myFocusColor
  ,defaultPrompter =  const "Firefox: "
  }
googleXPConfig = greenXPConfig {
  fgColor = "white"--myFocusColor
  ,  position =  CenteredAt {xpCenterY = 0.29 , xpWidth = 0.48}
  ,  font = "xft:Lucida MAC:size=22"
  , height            = 80
  }

youtubeXPConfig = googleXPConfig {
  fgColor = "red"--myFocusColor
  }

myGround = do
  bat <- runProcessWithInput "bash" []  "cat /sys/class/power_supply/BAT0/capacity"
  dat <- runProcessWithInput "bash" []  "date +'%d/%m/%Y'"
  day <- runProcessWithInput "bash" []  "date +'%A'"
  time <- runProcessWithInput "bash" []  "date +'%H:%M'"
  spawnSelected def{ gs_cellheight = 100,
                                                         gs_cellwidth = 300 , gs_colorizer = myColorizer , gs_font = "xft:Lucida MAC:size=30"} [ bat, dat,day,time]

nixosList = [
  -- ("update",spawn "home-manager switch --flake \"/home/vamshi/aios#cosmos\" && notify-send \"Successfully updated\" || notify-send -u critical \"Updating failed!!\" ")
  ("update",spawn "nix build \"/home/vamshi/aios#cosmos\" --impure ; nix run \"/home/vamshi/aios#cosmos\" --impure  && notify-send \"Successfully updated\" || notify-send -u critical \"Updating failed!!\" ")
  ,("packages",spawn $ myEditor <> " ~/aios/genPackages/default.nix")
  ,("xmonad",spawn $ myEditor <> " ~/aios/xsession/xmonad.hs")
  ,("emacs", spawn $ myEditor <> " ~/aios/editor/emacs/config.org")
  ,("flake", spawn $ myEditor <> " ~/aios/flake.nix")
            ]

appsList = [
  ("bluetooth",do
      -- spawn $ "pavucontrol  "
      -- spawn "sleep 15"
      -- kill
      spawn $ "pavucontrol ; " <> myTerminal <> " -e bluetoothctl")
  ,("editor",spawn myEditor )
  ,("browser",spawn myBrowser )
  ,("terminal",spawn "emacsclient -c -a '' --eval '(vterm)'")
  ,("file manager",spawn "emacsclient -c -a '' --eval '(dired nil)'")
  ,("zathura", spawn "zathura")
  -- ,("alacritty",spawn myTerminal)
  -- ,("alacritty",)
           ]

exitList = [
  ("shutdown",confirmPrompt youtubeXPConfig "Shutdown" $ spawn "poweroff")
  ,("reboot",confirmPrompt youtubeXPConfig "Reboot" $ spawn "reboot")
  ]

redList = [
  ("restart",spawn "systemctl --user restart redshift.service")
  ,("urestart",spawn "sudo nixos-rebuild switch ; systemctl --user restart redshift.service")
  ,("stop",spawn "systemctl --user stop redshift.service")
  ,("open",spawn $  myTerminal <> " -e sudo kak /etc/nixos/configuration.nix")
    ]
gsconfig2 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 100,
                                                         gs_cellwidth = 200 ,
                                                         gs_navigate = mynavNSearch,
                                                         gs_font = "xft:Lucida MAC:size=23"}

mynavNSearch = makeXEventhandler $ shadowWithKeymap navNSearchKeyMap navNSearchDefaultHandler
  where navNSearchKeyMap = M.fromList [
          ((0,xK_Escape) , cancel)
          ,((0,xK_space)     , select)
          ,((controlMask,xK_e)     , select)
          ,((0,xK_Return)     , select)
          ,((0,xK_Left)       , move (-1,0) >> mynavNSearch)
          ,((0,xK_Right)      , move (1,0) >> mynavNSearch)
          ,((0,xK_Down)       , move (0,1) >> mynavNSearch)
          ,((0,xK_Up)         , move (0,-1) >> mynavNSearch)
          ,((0,xK_Tab)        , moveNext >> mynavNSearch)
          ,((shiftMask,xK_Tab), movePrev >> mynavNSearch)
          ,((0,xK_BackSpace), transformSearchString (\s -> if s == "" then "" else init s) >> navNSearch)
          ]
        navNSearchDefaultHandler (_,s,_) = do
          transformSearchString (++ s)
          mynavNSearch

myTerm "alacritty" = "Alacritty"
myTerm "kitty" = "kitty"

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal -- ++ " -n scratchpad"
    findTerm   = className =?   myTerm myTerminal
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.50
                 w = 0.84
                 t = 0
                 l = 0.1
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnSpotify  =  "spotify"-- ++ " -n scratchpad"
    findSpotify   = className =? "Spotify"--myTerminal
    manageSpotify = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w

-- | A map from window names to Windows, given a windowTitler function.
windowMap'' :: (X.WindowSpace -> Window -> Int -> X String) -> X (M.Map String Window)
windowMap'' titler = do
  ws <- gets X.windowset
  M.fromList . concat <$> mapM keyValuePairs (W.workspaces ws)
  where keyValuePairs ws = mapM (keyValuePair ws) $ zip (W.integrate' (W.stack ws)) [0..]
        keyValuePair ws (w,num) = flip (,) w <$> titler ws w num

decorateName :: X.WindowSpace -> Window -> Int -> X String
decorateName ws w num = do
  name <- show <$> getName w
  return $ (show num) <> " " <> name <> " [" ++ W.tag ws ++ "]"

-- | A map from window names to Windows.
windowMap' :: X (M.Map String Window)
windowMap' = windowMap'' decorateName

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch navNSearch)--myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_w)     , moveNext     >> myNavigation)
         ,((mod4Mask,xK_w), moveNext     >> myNavigation)
         ,((mod4Mask,xK_e), select)
         ,((mod4Mask,xK_q), select)
         ,((0,xK_e), select)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         -- ,((mod4Mask,xK_w) , select)--setPos (0,0) >> myNavigation)
         ,((0,xK_space) , select)--setPos (0,0) >> myNavigation)
         ,((0,xK_s) , select)--setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation


alttab = goToSelected def {gs_cellheight = 100,gs_cellwidth=390,gs_font="xft:Lucida MAC:size=20",gs_navigate = myNavigation,gs_colorizer = myColorizer'}

myColorizer' :: a -> Bool -> X (String,String)
myColorizer' _ True = return ("#46eed9","#000000")
myColorizer' _ False = return ("#000000","#dddddd")


myColorizer :: a -> Bool -> X (String,String)
myColorizer _ True = return ("#46d9ee","#000000")
myColorizer _ False = return ("#000000","#dddddd")
